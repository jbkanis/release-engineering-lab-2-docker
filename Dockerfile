FROM ubuntu:latest

COPY version.txt /root/version.txt
ENTRYPOINT ["cat"]
CMD ["/root/version.txt"]
